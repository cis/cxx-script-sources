// file: Stringeinsatz/stringLexComp.cpp
// description:

#include <algorithm>
#include <iterator>
#include <iostream>
#include <string>

using namespace std;

int main() {

    string word1 = "alpha";
    string word2 = "beta";
    string word3 = "gamma";


    cout << word1 << " steht " <<
            //Vergleich von word1, word2; Ausgabe
            (lexicographical_compare(word1.begin(), word1.end(),
            word2.begin(), word2.end()) ?
            "alphabetisch vor "
            :
            "nach oder an der gleichen Stelle wie "
            ) << word2 << endl;



    cout << word1 << " steht " <<
            //Vergleich von word1, word3; Ausgabe
            (lexicographical_compare(word1.begin(), word1.end(),
            word3.begin(), word3.end()) ?
            "alphabetisch vor "
            :
            "nach oder an der gleichen Stelle wie "
            ) << word3 << endl;



    cout << word2 << " steht " <<
            //Vergleich von word2, word3; Ausgabe
            (lexicographical_compare(word2.begin(), word2.end(),
            word3.begin(), word3.end()) ?
            "alphabetisch vor "
            :
            "nach oder an der gleichen Stelle wie "
            ) << word3 << endl;

    return 0;

}
