// file: Stringeinsatz/stringInit.cpp
// description: work with Strings  

#include <iostream>
#include <string>

using namespace std;

int main() {
    string str1 = "abc";
    string str2("String 2");
    string str3(10, 'c');

    cout << " Hier ist das Programm String_init " << endl;
    cout << " Der Initialwert war: " << str1 << endl;
    cout << " Der Initialwert war: " << str2 << endl;
    cout << " Der Initialwert war: " << str3 << endl;

    if (str1 == "abc") {
        string conc;
        conc = str1 + "defghi";
        cout << " Die Konkatenation = " << conc << endl;
    }
    return 0;

}
