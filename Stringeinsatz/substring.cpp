// file: Stringeinsatz/substring.cpp
// description: work with Strings 

#include <iostream>
#include <string> 

using namespace std;

int main() {
    string str1, str2;
    int pos;

    cout << " Hier ist das Programm Substring " << endl;
    cout << " Bitte geben Sie einen String ein >>>";
    cin >> str1;
    cout << " Bitte geben Sie einen zweiten String ein >>>";
    cin >> str2;

    cout << " Eingabe war: " << str1 << "  " << str2 << endl;

    pos = str1.find(str2);

    if (pos != string::npos)
        cout << " 2. String beginnt an Pos. " << pos << endl;
    return 0;
}
