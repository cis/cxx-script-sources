// file: Stringeinsatz/searchNpos.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int main() {
    char buchstabe;
    string vokal = "aeiou";

    cout << " Hier ist Programm searchNpos " << endl;
    cout << " Bitte einen Buchstaben eingeben: ";
    cin >> buchstabe;

    cout << " Die Eingabe war: " << buchstabe << endl;

    if (vokal.find(buchstabe) != string::npos)
        cout << " Der Buchstabe ist ein Vokal " << endl;
    else
        cout << " Der Buchstabe ist kein Vokal " << endl;

    return 0;
}