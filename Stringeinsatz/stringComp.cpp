// file: Stringeinsatz/stringComp.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int main() {

    string string1("Hello world");
    string string2("Hello yourself");
    string string3("Hello world");




    if (string1.compare(string2) > 0)
        cout << string1 << ", alphabet. nach " << string2 << endl;
    else if (string1.compare(string2) < 0)
        cout << string1 << ", alphabet. vor " << string2 << endl;
    else
        cout << string1 << " und " << string3 << " sind gleich \n";


    if (string1.compare(string3) > 0)
        cout << string1 << ", alphabet. nach " << string3 << endl;
    else if (string1.compare(string3) < 0)
        cout << string1 << ", alphabet. vor " << string3 << endl;
    else
        cout << string1 << " und " << string3 << " sind gleich \n";


    if (string2.compare(string3) > 0)
        cout << string2 << ", alphabet. nach " << string3 << endl;
    else if (string2.compare(string3) < 0)
        cout << string2 << ", alphabet. vor " << string3 << endl;
    else
        cout << string2 << " und " << string3 << " sind gleich \n";

    return 0;

}
