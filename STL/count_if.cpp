// filename: STL/count_if.cpp
// description: 

#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>

using namespace std;

bool greater7(int);

int main() {
    ostream_iterator< int > output(cout, " ");
    int a2[ 10 ] = {99, 27, 13, 7, 5, 27, 3, 87, 93, 7};
    vector< int > v2(a2, a2 + 10); // Kopie von a2
    cout << "Der Vektor enthaelt: ";
    copy(v2.begin(), v2.end(), output);
    int r = count_if(v2.begin(), v2.end(), greater7);
    cout << " \nAnzahl der Elemente groesser 7: " << r;
    cout << endl;
    return 0;
}

bool greater7(int value) {
    return value > 7;
}
