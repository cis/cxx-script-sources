// File: unordered_map.cxx
// g++ -std=c++0x unordered_map.cpp
// starten: ./a.out <filename>

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>      
#include <stdlib.h>

using namespace std;

// Definition of the HashMap Template, C++09 Standard
typedef unordered_map<string, int> HashMap;

int main(int argc, char *argv[]) {
    HashMap str_hmap;
    HashMap::iterator pos;

    if (argc == 1) {
        cout << "./a.out <filename>" << endl;
        exit(1);
    }
    ifstream infile(argv[1]);
    string line;

    while (getline(infile, line)) {
        cout << " gelesen :" << line << endl;
        if (!str_hmap[line]) {
		//oder:	if (str_hmap.find(line) == str_hmap.end())
            cout << "New->" << line << "<-" << endl;
            str_hmap[line] = 1;
            //oder: str_hmap.insert(pair<string,int>(line,1));

        }
        else {
            ++str_hmap[line];
            //pos = str_hmap.find(line);
            //pos->second++;
            cout << "Upd->" << line << "<- = " << str_hmap[line] << endl;
        }
    }
    cout << "File is read" << endl << endl << endl;
    cout << "Content of the HASH " << endl;

    for (pos = str_hmap.begin(); pos != str_hmap.end(); ++pos) {
        cout << "#" << pos->first << "#  = " << pos->second << endl;
    }

    return 0;

}


