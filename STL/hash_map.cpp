// file: STL/hash_map.cpp
// description: Implementation einer map mit einer Hashunktion

#include <iostream>
#include <fstream>
#include <string>
#include <ext/hash_map>

using namespace std;
using namespace __gnu_cxx;

class HashStringEqual {
public:

    bool operator()(const string &S1,
            const string &S2) const {
        return S1 == S2;
    }
};

class HashStringFunction {
public:

    int operator()(const string &Str) const {
        return hash<char const *>()(Str.c_str());
    }
};

typedef hash_map<string, int, HashStringFunction,
HashStringEqual > MyMap;

int main() {
    MyMap string_map;
    MyMap::iterator pos;
    ifstream infile("eingabe.txt");
    string line;

    while (getline(infile, line)) {
        cout << " gelesen :" << line << endl;
        if (!string_map[line]) {
            cout << " insert: " << line << endl;
            string_map[line] = 1;
        } else {
            ++string_map[line];
            cout << " update: " << line << " New count: ";
            cout << string_map[line] << endl;
        }
    }

    for (pos = string_map.begin(); pos != string_map.end(); ++pos) {
        cout << " Element: " << pos->first << " Anzahl: ";
        cout << pos->second << endl;
    }

    return 0;
}
