// autor: max
// file:unordered_set.cxx
// g++ -std=c++0x unordered_set.cpp

#include <iostream>
#include <fstream>
#include <string>
#include <unordered_set>

using namespace std;

// Definition of the HashSet Template, C++09 Standard
typedef unordered_set<string>  HashSet;

int main()
{
  HashSet string_set;
  HashSet::iterator pos;

  ifstream infile ("eingabe.txt");
  string line;

  while (getline(infile,line)) {
      cout << " gelesen :" << line << endl;
      string_set.insert(line); // oder string_set[line]
    }
  cout << " Alles gelesen " << endl;

  for (pos = string_set.begin();pos != string_set.end(); ++pos) {
      cout << " Element = " << *pos << endl;
    }

  return 0;
}
