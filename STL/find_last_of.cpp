// file: STL/find_last_of.cpp
// description: 

#include <iostream>
#include <string>

using namespace std;

int main() {

    string string1("Dies ist ein Test!");
    int location;

    location = string1.find_last_of("ist");
    cout << "\nfind_last_of() hat '" << string1[location]
            << "' gefunden an Pos: " << location << endl;

    return 0;
}
