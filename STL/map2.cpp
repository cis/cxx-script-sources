// file: STL/map2.cpp
// descrition:

#include <map>
#include <string>
#include <iostream>
#include <utility> 

using namespace std;

typedef multimap<int, string> MULTI_MAP;
typedef multimap<int, string>::iterator ITERATOR;

int main() {
    MULTI_MAP m; //Anlegen der Multimap
    ITERATOR pos; //Iterator


    m.insert(pair<int, string > (1, "Hans"));
    m.insert(pair<int, string > (1, "Helmut"));
    m.insert(pair<int, string > (3, "Herbert"));
    m.insert(pair<int, string > (7, "Hubert"));

    cout << "Hier die Multimap: " << endl;

    //for-Schleife mit dem Iterator pos;
   for (pos = m.begin(); pos != m.end(); ++pos) 
        //cout << pos->first << " " << pos->second << endl;
        cout << (*pos).first << " " << (*pos).second << endl;
    cout << endl;

    pos = m.find(3); //Paar zum Schl�ssel suchen
    if (pos != m.end())
        cout << m.first << " " << m.second << endl;

    int key = 1; //Anzahl der Objekte bestimmen:
    cout << "Zum Schuessel " << key << " gibt es ";
    cout << m.count(key) << " Objekte " << endl;

    return 0;
}
