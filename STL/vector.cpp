// file: STL/vector.cpp
// description: Beispiel mit typedef und Iteratoren

#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
    vector<string> worte;
    string eingabe;
    int i = 0;

    cout << " Hello Bitte geben Sie einen String ein >>>";
    while (cin >> eingabe) {
        worte[i] = eingabe;
        i++;
        cout << " Naechsten String eingeben oder ^D >>>";
    }

    for (i = 0; i < worte.size(); i++)
        cout << " Element[ " << i << " ] = " << worte[i] << endl;

}
