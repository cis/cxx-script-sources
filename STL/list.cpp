// file: STL/list.cpp
// description:

#include <iostream>
#include <fstream>
#include <string>
#include <list>

using namespace std;

typedef list <string> MyList;

int main() {
    MyList strList;
    MyList::iterator pos;
    int i;
    ifstream infile("eingabe.txt");
    string line;

    while (getline(infile, line)) {
        cout << " gelesen :" << line << endl;
        strList.push_back(line);
    }
    cout << " Alles gelesen " << endl;

    for (pos = strList.begin(); pos != strList.end(); ++pos) {
        cout << " Element = " << *pos << endl;
    }
    // oder
    //for (i = 0; i < strList.size(); i++)
    //    cout << " Element[" << i << "]= " << strList[i] << endl;
    //return 0;
}
