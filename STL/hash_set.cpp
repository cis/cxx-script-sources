// file: STL/hash_set.cpp
// description: Implementation eines set mit einer Hashfunktion

#include <iostream>
#include <fstream>
#include <string>
#include <ext/hash_set>

using namespace std;
using namespace __gnu_cxx;

class HashStringEqual {
public:

    bool operator()(const string &S1,
            const string &S2) const {
        return S1 == S2;
    }
};

class HashStringFunction {
public:

    int operator()(const string &Str) const {
        return hash<char const *>()(Str.c_str());
    }
};



typedef hash_set<string, HashStringFunction, HashStringEqual > MySet;

int main() {
    MySet string_set;
    MySet::iterator pos;

    ifstream infile("eingabe.txt");
    string line;

    while (getline(infile, line)) {
        cout << " gelesen :" << line << endl;
        string_set.insert(line); // oder string_set[line]
    }
    cout << " Alles gelesen " << endl;

    for (pos = string_set.begin(); pos != string_set.end(); ++pos) {
        cout << " Element = " << *pos << endl;
    }

    return 0;
}
