// file: STL/mapCount.cpp
// description:

//#include <stdlib.h>

#include <iostream>
#include <fstream>
#include <map>
#include <string>

using namespace std;

typedef map<string, int> AssocativeArray;

////////////////// functions 
int myChomp(string &str);

int main() {

    fstream fs;
    ifstream infile("wordlist.txt");
    string word;
    string token;
    AssocativeArray tokenData;
    AssocativeArray::iterator im;

    //  Read each line: every line: one word !!
    while (getline(infile, token)) {
        myChomp(token); // remove carriage return at the end
        cout << "gelesen " << token;
        if (tokenData[token]) {
            //        update value
            cout << " und gefunden " << endl;
            tokenData[token]++;
        } else {
            //        Store token and set value to 1
            cout << " und nicht gefunden " << endl;
            tokenData[token] = 1;
        }

    }

    // Print out all tokens in the map container
    for (im = tokenData.begin(); im != tokenData.end(); ++im) {
        cout << " Element = #" << im->first << "#" << endl;
        cout << " Value = " << im->second << endl;
    }

    // Now read Elements from the MAP container
    do {
        cout << "Enter word to seach in the map >>> ";
        getline(cin, word);

        if (word != "") {
            //      Use the find function to get an iterator position
            im = tokenData.find(word);
            if (im != tokenData.end()) {
                cout << " Found \"" << im->first;
                cout << "\"  = " << im->second << endl;
            } else {
                cout << "tokenData.find(...) no token matches \"";
                cout << token << "\"" << endl;
                cout << "Lookup tokenData[\"" << word;
                cout << "\"] would have given ";
                cout << tokenData[word] << endl;
            }
        }
    } while (word != "");

    return 0;

}

int myChomp(string &str) {
    int lastCharPos = str.length() - 1;
    if (str[lastCharPos] == '\r' || str[lastCharPos] == '\n')
        str.resize(lastCharPos);
}
