// file: STL/hash_mapUTF8.cpp
// description: Works with utf-8 Input

#include <iostream>
#include <fstream>
#include <string>
#include <locale>
#include <ext/hash_map> 

using namespace std;

// namespace of hash-functions varies from one compiler and gcc version
// in gcc v3.1/v3.2 and later: namespace __gnu_cxx.
// see: http://gcc.gnu.org/onlinedocs/libstdc++/faq/index.html#5_4 
// Here it is renamed to stdext.
// Namespace alias to catch hash_map classes

namespace stdext = ::__gnu_cxx;


// Auxiliary memory and Function for the Hash function: 
// Task: Convert wstring to char buffer 
// buffer: 
// static buffer for conversion
#define BYTEBUFSIZE 1024
static char utf8bytes[BYTEBUFSIZE];

// function header:
int into_byte_buf(wstring ws, char *buf, int buf_len);

// For containers like hash_map the equal & hash classes must be defined!!!
// This class' function operator() generates a hash value for a key.
// Unique hash values (indices) give quickest access to data.

class HashWStringFunction {
public:

    int operator()(const wstring &WStr) const {
        into_byte_buf(WStr, utf8bytes, BYTEBUFSIZE);
        return stdext::hash<const char*>() (utf8bytes);
    }
};

// This class' function operator() tests if any two keys are equal.

class HashWStringEqual {
public:

    bool operator()(const wstring &S1,
            const wstring &S2) const {
        return S1 == S2;
    }
};



/////////////////////////////////
// Definition of the HashMap Template
// using namespace stdext   (Standard Extensions)
typedef stdext::hash_map<wstring, int, HashWStringFunction,
HashWStringEqual> WHashMap;

int main() {
    WHashMap wstring_hashmap;
    WHashMap::iterator pos;

    wstring line;
    // Construct locale object with the user's default preferences:
    locale mylocale("de_DE.utf-8");
    wcout.imbue(mylocale); //  wcout mit dem locale 'einf�rben'
    locale::global(mylocale); // das Locale global machen

    wifstream infile("eingabe_utf8.txt");
    infile.imbue(mylocale);

    while (getline(infile, line)) {
        if (!wstring_hashmap[line]) {
            wcout << "new-->" << line << "<--" << endl;
            wstring_hashmap[line] = 1;
        } else {
            ++wstring_hashmap[line];
            wcout << "upd-->" << line << "<-- New count= ";
            wcout << wstring_hashmap[line] << endl;
        }
    }
    wcout << "File is read" << endl << endl << endl;
    wcout << "Content of the HASH " << endl;

    for (pos = wstring_hashmap.begin();
            pos != wstring_hashmap.end(); ++pos) {
        wcout << "#" << pos->first << "#  = " << pos->second << endl;
    }
    return 0;

}

// Converts a wstring into a zero terminated byte Buffer

int into_byte_buf(wstring ws, char *buf, int buf_len) {
    // Input:
    //     wstring ws ... wide string to convert
    //     buf_len    ... Number of Bytes of the output Buffer buf
    // Output:
    // char *buf      ... Pointer to a buffer with buf_len bytes, 
    //                    to hold the result
    //
    int i, j;
    int index = 0;
    int buf_len_m1 = buf_len - 1;

    union wstr_byte {
        unsigned int zahl32;
        unsigned char buf8[4];
    } u;

    j = 0;
    while (j < ws.size() && index < buf_len_m1) {
        u.zahl32 = ws[j];
        i = 0;
        while (u.buf8[i] && index < buf_len_m1) {
            buf[index] = u.buf8[i];
            index++;
            i++;
        }
        j++;
    }
    buf[index] = 0;
}
