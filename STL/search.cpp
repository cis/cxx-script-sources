// file: STL/search.cpp
// description:

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

bool myPredicate(int i, int j) {
    return (i == j);
}

int main() {
    vector<int> myVec;
    vector<int>::iterator it;

    for (int i = 1; i < 10; i++) myVec.push_back(i * 10);
    //Vektor: 10 20 30 40 50 60 70 80 90

    //mit normalem Vergleich:
    int match1[] = {40, 50, 60, 70};
    it = search(myVec.begin(), myVec.end(), match1, match1 + 4);

    if (it != myVec.end()) {
        cout << "match1 gefunden, Position ";
        cout << int(it - myVec.begin()) << endl;
    } else
        cout << "match1 nicht gefunden" << endl;

    //Vergleich mit Praedikat:
    int match2[] = {20, 30, 50};
    it = search(myVec.begin(), myVec.end(), match2, match2 + 3, myPredicate);

    if (it != myVec.end())
        cout << "match2 gefunden, Position " << int(it - myVec.begin()) << endl;
    else
        cout << "match2 nicht gefunden" << endl;
    return 0;
}
