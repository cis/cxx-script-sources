// file: STL/map1.cpp
// description:

#include <iostream>
#include <fstream>
#include <string>
#include <map>

using namespace std;

typedef map<wstring, int> MyMap;

int main() {
    MyMap stringMap;
    MyMap::iterator pos;
    
    setlocale(LC_CTYPE,"");

    wifstream infile("eingabe.txt");
    wstring line;
    locale utf8 = locale("de_DE.UTF-8");
    infile.imbue(utf8);

    while (getline(infile, line)) {
        
        wcout << L" gelesen :" << line << endl;
        stringMap[line]++;
    }
    wcout << " Alles gelesen " << endl;

    for (pos = stringMap.begin(); pos != stringMap.end(); ++pos) {
        wcout << L" Element = " << pos->first << endl;
        wcout << L" Value = " << pos->second << endl;
    }
    return 0;
}
