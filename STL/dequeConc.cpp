// file: STL/dequeConc.cpp
// description: Concordance mit deque

#include <fstream>
#include <iostream>
#include <deque>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

typedef deque<string> MyDeque;
typedef vector<string> MyVec;

const int DequeSize = 5;

//////////// FUNCTIONS:
int fillDeque(MyDeque &wordDeque, string &word);
int examineDeque(MyDeque &wordDeque, string &searchWord);
bool alnum(char c);
bool notAlnum(char c);
int split(MyVec &words, string &line);
int printFormattedDeque(MyDeque &wordDeque);

/////////////// MAIN

int main() {

    int rline, lnum;
    string filename = "eingabe.txt";
    fstream fs;
    string word, line, nextWord, searchWord;
    int i, numOfWords;

    MyDeque wordDeque(DequeSize);
    MyVec words;

    //  End of declarations ...

    cout << " Hello concordance with deque " << endl;
    cout << " Input from Textfile=" << filename << endl;

    //  Open the file for reading
    cerr << " Open File " << filename << endl;
    fs.open(filename.c_str(), ios::in);
    if (!fs) {
        cerr << " Fileopen Error on " << filename << endl;
        exit(-1);
    }

    cout << " Enter Concondance word " << endl;
    cin >> searchWord;

    //  Read each line from the file, split it, and insert it
    do {
        if (getline(fs, line)) {
            split(words, line);
            for (i = 0; i < words.size(); i++) {
                fillDeque(wordDeque, words[i]);
                examineDeque(wordDeque, searchWord);
            }
        }
        words.clear();
    } while (!fs.eof());
    return ( EXIT_SUCCESS);
}


//////////////////////////////////////////

int fillDeque(MyDeque &wordDeque, string &nextWord) {
    // append to deque
    wordDeque.push_back(nextWord);
    // Delete first Element from deque
    wordDeque.erase(wordDeque.begin());
    return 1;
}

////////////////////////////////////

int examineDeque(MyDeque &wordDeque, string &searchWord) {
    int i = 0;
    char numOfChars = 0;

    if (searchWord == wordDeque[DequeSize / 2]) {
        /*      for (i=0; i < DequeSize;++i)
         {
                 cout  << wordDeque[i] << " " ;
         }
        cout << endl;
         */
        printFormattedDeque(wordDeque);
    }
    return 1;
}
////////////////////////////////////

int split(MyVec &result, string &line) {
    string::iterator from, to;
    string word;

    from = line.begin();
    while (from != line.end()) {
        from = find_if(from, line.end(), alnum);
        to = find_if(from, line.end(), notAlnum);

        if (from != line.end()) {
            result.push_back(string(from, to));
        }
        from = to;
    }
    return 1;
}

////////////////////////////////////

bool notAlnum(char c) {
    return !isalnum(c);
}

////////////////////////////////////

bool alnum(char c) {
    return isalnum(c);
}

////////////////////////////////////

int printFormattedDeque(MyDeque &wordDeque) {
    int i, numOfChars = 0;
    int numOfColumns = 50;

    cout << "|";
    for (i = 0; i < DequeSize / 2; i++) {
        if (wordDeque[i] != "") {
            cout << wordDeque[i] << " ";
            numOfChars += wordDeque[i].size();
        } else {
            cout << "-";
        }
    }
    cout << " ++" << wordDeque[DequeSize / 2] << "++  ";
    numOfChars += wordDeque[DequeSize / 2].size();
    for (i = DequeSize / 2 + 1; i < DequeSize; ++i) {
        cout << wordDeque[i] << " ";
        numOfChars += wordDeque[i].size();
    }
    while (numOfChars < numOfColumns) {
        cout << "-";
        ++numOfChars;
    }

    cout << "|" << endl;
}
