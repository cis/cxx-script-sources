// file: STL/makeP.cpp
// description:

#include <map>
#include <string>
#include <iostream>
#include <utility> 
#include <algorithm>

using namespace std;

typedef multimap<int, wstring> wstring_map;
typedef wstring_map::iterator it;

int main() {
    wstring_map m; //Anlegen der Multimap
    it pos; //Iterator
    

    m.insert(wstring_map::value_type(1, L"Hans"));
    m.insert(wstring_map::value_type (1, L"Helmut"));
    m.insert(wstring_map::value_type (3, L"Herbert"));
    m.insert(wstring_map::value_type (7, L"Hubert"));

    wcout << L"Hier die String- Multimap: " << endl;

    for (pos = m.begin(); pos != m.end(); ++pos)
        wcout << pos->first << L" " << pos->second << endl;

    wcout << endl;

    pos = m.find(3); //Paar zum Schluessel suchen
    if (pos != m.end())
        wcout << pos->first << L" " << pos->second << endl;

    int key = 1; //Anzahl der Objekte bestimmen:
    wcout << L"Zum Schluessel " << key << L" gibt es ";
    wcout << m.count(key) << L" Objekte " << endl;

    return 0;
}
