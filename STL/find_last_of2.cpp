// file: STL/find_last_of2.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int main() {

    string lower("abcdefghijklmnopqrstuvwxyz");
    string upper("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    //string any(lowerChar + upperChar + ' ');
    string street("Bogenstrasse 17");

    cout << "last UCase: " << street.find_last_of(upper) << endl;
    cout << "last LCase: " << street.find_last_of(lower) << endl;
    return 0;
}
