// file: STL/vectorIt.cpp
// description: Beispiel mit typedef und Iteratoren

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

typedef vector <string> MyVector;

int main() {
    MyVector strVec;
    MyVector::iterator pos;
    int i;

    ifstream infile("eingabe.txt");
    string line;

    while (getline(infile, line)) {
        cout << " gelesen :" << line << endl;
        strVec.push_back(line);
    }
    cout << " Alles gelesen " << endl;

    for (pos = strVec.begin(); pos != strVec.end(); ++pos) {
        cout << " Element = " << *pos << endl;
    }

    // oder

    for (i = 0; i < strVec.size(); i++)
        cout << " Element[" << i << "]= " << strVec[i] << endl;


    return 0;

}
