// file: STL/set.cpp
// description:

#include <iostream>
#include <fstream>
#include <string>
#include <set>

using namespace std;

typedef set<string> MySet;

int main() {
    MySet strSet;
    MySet::iterator pos;

    ifstream infile("wordlist.txt");
    string line;

    while (getline(infile, line)) {
        cout << " gelesen :" << line << endl;
        strSet.insert(line);
    }
    cout << " Alles gelesen " << endl;

    for (pos = strSet.begin(); pos != strSet.end(); ++pos) {
        cout << " Element = " << *pos << endl;
    }
    return 0;
}
