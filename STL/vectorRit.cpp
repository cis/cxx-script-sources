// file: STL/vectorRit.cpp
// description:

#include <iostream>
#include <vector>
using namespace std;

int main ()
{
  vector<int> myvector;
  
  // Speichert Werte von 1 bis 10
  for (int i=1; i<=10; i++) {
	  myvector.push_back(i);
  }

  vector<int>::reverse_iterator rit;
  for ( rit=myvector.rbegin() ; rit < myvector.rend(); ++rit ) {
    cout << " " << *rit;
  }
  
  return 0;
}
