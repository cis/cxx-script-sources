// file: STL/search_n.cpp
// description:

#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

bool myPred(int i, int j) {
    return (i == j);
}

int main() {
    int myInts[] = {10, 20, 30, 30, 20, 10, 10, 20};
    vector<int> myVec(myInts, myInts + 8);

    vector<int>::iterator it;

    //mit normalem vergleich:
    it = search_n(myVec.begin(), myVec.end(), 2, 30);

    if (it != myVec.end()) {
        cout << "zwei mal 30 gefunden an Position ";
        cout << int(it - myVec.begin()) << endl;
    } else
        cout << "nicht gefunden" << endl;

    // Vergleich mit Praedikat:
    it = search_n(myVec.begin(), myVec.end(), 2, 10, myPred);

    if (it != myVec.end()){
        cout << "zwei mal 10 gefunden an Position ";
    cout << int(it - myVec.begin()) << endl;
    } else
        cout << "nicht gefunden" << endl;

    return 0;
}
