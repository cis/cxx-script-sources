// file: Vererbung/wort.hpp
// description:

#ifndef WORT_HPP
#define WORT_HPP

#include <string>

class Wort {
public:
    Wort(std::wstring);
    int laenge();
    const std::wstring asString();
private:
    std::wstring text;
};

Wort::Wort(std::wstring text) {
    this -> text = text;
}

int Wort::laenge() {
    return this -> text.length();
}

const std::wstring Wort::asString() {
    return this -> text ;
}

#endif /* WORT_HPP */
