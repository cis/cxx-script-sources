// file: Vererbung/tier.hpp
// description: 

#ifndef TIER_HPP
#define TIER_HPP

#include <string>
#include <iostream>
#include <stdlib.h>

class Tier {
public:
    Tier(std::wstring tierName):name(tierName) {
        geschlecht = geschlechtBestimmen();
    }

    void eigenheiten();
	
	void macht();
	void rufen();
	void istEin();
    
protected:
    std::wstring name;
    std::wstring geschlecht;
    std::wstring laut;
    std::wstring geschlechtBestimmen();
};

void Tier::eigenheiten() {
    std::wcout << L"Mein Tiername ist: " << name << std::endl;
}

void Tier::rufen() {
    std::wcout << this->name << L" komm her!" << std::endl;
}

void Tier::macht() {
    std::wcout << this->laut << std::endl;
}

void Tier::istEin() {
    std::wcout << this->name << L" ist ein " << this->geschlecht << std::endl;
}

std::wstring Tier::geschlechtBestimmen() {
    if ( rand() %2 == 0) {
        return L"Weiblich";
    }
    else {
       return L"Männlich";
    }
}

#endif /* TIER_HPP */
