// file: Vererbung/EselVirt.hpp
// description: 

#ifndef ESEL_HPP
#define ESEL_HPP

#include "tierVirt.hpp"

class Esel : public Tier {
public:
	Esel(std::wstring name) : Tier(name) {
        laut = L"Iah! Iah! Iah!";
    }
    
    void eigenheiten();
	
};

void Esel::eigenheiten() {
	std::wcout << L"Ich bin ein Esel und heisse " << this->name << std::endl;
	std::wcout << L"Ich kann einen tollen Laut machen: " << this->laut << std::endl;
	std::wcout << L"Und ich bin furchtbar sturr!" << std::endl;
}

#endif /* ESEL_HPP */
