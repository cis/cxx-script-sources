// file: Vererbung/text.hpp
// description: 

#ifndef TEXT_HPP
#define TEXT_HPP

#include <vector>
#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include "wort.hpp"

class Text {
public:
    Text();
    Text(std::string);
    Text(std::string, const std::wstring);
    void printOnTerminal();
    std::vector<Wort> getText();
    std::wstring getName();
    Text & operator=(Text);
    bool find(std::wstring);
    void findAndPrint(std::wstring);
    
private:
    std::wstring name;
    std::vector<Wort> text;
    std::map< std::wstring, std::vector<int> > wordPositions;
    void readWordsfromFile( std::wifstream &);
    void readWordsFromString( std::wstring &);
    void readWordsFromStream( std::wistream &);
    void buildPositionsMap(std::vector<Wort> &);
    void buildPositionsMapAlternativeSyntax(std::vector<Wort> &);
    void printConcordance(int, int);
    std::wstring string2wstring(std::string);

};

// Constructor
Text::Text() {
}

/*
Text aus einer Datei erstellen.
*/
Text::Text(std::string filename) {
    this -> name = string2wstring(filename);
    std::wifstream file(filename.c_str());
    readWordsfromFile(file) ;
}

/*
Text aus einem String erstellen. Name des Textes ist das erste Argument.
*/
Text::Text(std::string name, std::wstring textAsString) {
    this -> name = string2wstring(name);
    readWordsFromString(textAsString);
    
}

/*
W�rter aus eine FileStream lesen.
*/

void Text::readWordsfromFile(std::wifstream & file) {
    // Zeile auf dem Mac auskommentieren.
    file.imbue(std::locale("de_DE.UTF-8"));
    if (file.good()) {
        readWordsFromStream(file);
    }
    else {
        std::wcout << "Problem mit der Datei" << std::endl;
    }
}

/*
W�rter aus einem StringStream lesen.
*/

void Text::readWordsFromString(std::wstring & textString) {
    std::wistringstream stringAsStream(textString);
    readWordsFromStream(stringAsStream);
}

/*
W�rter von einem Stream lesen
*/

void Text::readWordsFromStream(std::wistream & inStream) {
    std::wstring wortString;
    while (inStream >> wortString) {
        text.push_back( Wort(wortString) );
    }
    buildPositionsMap(text);
}

/*
Container Map mit Wortpositionen f�r Konkordanz erstellen.
*/
void Text::buildPositionsMap(std::vector<Wort> & words) {
    std::vector<Wort>::iterator i;
    
    for ( i= words.begin(); i != words.end(); i++ ) {
        std::wstring wortString = i -> asString();
        int iteratorPos = distance(words.begin(),i);
        
        // Es gibt schon einen Eintrag
        if (wordPositions.find( wortString ) != wordPositions.end()) {
            // Vector dereferenzieren
            wordPositions.at(wortString).push_back( iteratorPos );
        }
        // Es gibt noch keinen Eintrag
        else {
            std::vector<int> v;
            v.push_back(iteratorPos);
            
            wordPositions.insert( std::map< std::wstring,std::vector<int> >::value_type(wortString,v) );
        }
    }
}

/*
Wort innerhalb des Textes finden. und Position ausgeben.
*/

void Text::findAndPrint(std::wstring wortString) {
    if( wordPositions.find(wortString) != wordPositions.end()) {
        std::vector<int> v = wordPositions.at(wortString);
        std::vector<int>::iterator i;
        
        for (i = v.begin() ;i != v.end(); i++) {
            std::wcout << L"\t" << wortString <<": gefunden an Position -> " << * i << L": ";
            printConcordance(* i, 3);
        }
        
    }
    else {
       std::wcout << L"Wort: " << wortString << L" ist in Text: ";
        std::wcout << name;
        std::wcout << L" nicht enthalten."<< std::endl;
    }
}

bool Text::find(std::wstring wortString) {
    if( wordPositions.find(wortString) != wordPositions.end()) {
        return true;
    }
    else {
        return false;
    }
}

std::wstring Text::getName() {
    return this -> name;
}

/*
Konkordanz drucken.
*/
void Text::printConcordance(int textPosition, int windowSize) {
    int begin = textPosition - windowSize;
    int end = textPosition + windowSize;
    
    if (textPosition - windowSize < 0) {
        begin = 0 ;
    }
    
    if ( textPosition + windowSize >= text.size() ) {
        end = text.size() -1 ;
    }
    
    while (begin <= end) {
        if (begin != textPosition) {
            std::wcout << text.at(begin).asString() << " ";
        }
        else {
            std::wcout << L"\t>" << text.at(begin).asString() << L"<\t";
        }
        begin++;
    }
    std::wcout << std::endl;
    
}

Text & Text::operator=(Text other) {
    this -> text = other.text;
    this -> name = other.name;
    return * this;
}

/*
Ganzen Text auf Terminal ausgeben
*/
void Text::printOnTerminal() {
    
    std::wcout << L"Text: \"" << name << L"\"" << std::endl;
    
    std::vector<Wort>::iterator i = text.begin();
    while (i != text.end() ) {
        std::wcout << i -> asString() << L" ";
        i++;
    }
    
    std::wcout << std::endl << std::endl;
}

std::wstring Text::string2wstring(std::string s) {
    std::wstring sWide(s.length(), L' ');
    std::copy(s.begin(), s.end(), sWide.begin());
    return sWide;
}

#endif /* TEXT_HPP */
