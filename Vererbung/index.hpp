// file: Vererbung/index.hpp
// description: 

#ifndef INDEX_HPP
#define INDEX_HPP
#include "text.hpp"

using namespace std;

class Index {
public:
    Index();
    void add(Text);
    bool find(std::wstring);
private:
    std::vector<Text> texts;

};

Index::Index() {
}

/*
Text zum Index hinzufügen.
*/
void Index::add(Text t) {
    texts.push_back(t);
}

/*
Token in Text finden.
*/
bool Index::find(wstring wortString) {
    vector<Text>::iterator i;
 
    bool nicht_im_index = true;
    
    for (i = texts.begin(); i != texts.end(); i++) {
        if (i ->find(wortString) ) {
            wcout << endl << L"Gefunden in Text: " << i->getName() << endl;
            i->findAndPrint(wortString);
            nicht_im_index = false;
        }
    
    }
    
    if ( nicht_im_index) {
        wcout << endl << wortString << L": Nicht im Index!" << endl << endl;
    }
}

#endif /* INDEX_HPP */
