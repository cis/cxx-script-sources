// file: Vererbung/mainEsel.cpp
// description: 

#include <cstdlib>
#include "Esel.hpp"

using namespace std;

int main() {
    
    setlocale(LC_ALL,"");
    
    Esel e(L"Mister Sturr");
    e.eigenheiten();
    
    return 0;
}
