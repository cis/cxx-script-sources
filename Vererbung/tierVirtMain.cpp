// file: Vererbung/tierVirtMain.cpp
// description: 

#include "EselVirt.hpp"
#include "tierVirt.hpp"

using namespace std;

int main() {
    
    setlocale(LC_ALL,"");
    
    Tier t(L"Tierchen");
    Esel e(L"Sturrchen");
    
    Tier * ptt;
    
    ptt = &e;
    ptt->eigenheiten();
    
    ptt = &t;
    ptt->eigenheiten();
}
