// file: Funktionen/defaultValues.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

// Deklaration mit Prototyp
bool bestArt(string wort, string sprache = "de");
bool bestArt(string wort, int maxChars, string sprache = "de");

bool bestArt(string wort, string sprache) {
    if (sprache == "de") {
        if (wort == "der" || wort == "die" || wort == "das") {
            cout << " Deutscher Artikel = " << wort << endl;
            return true;
        }
    }
    if (sprache == "en") {
        if (wort == "the") {
            cout << " Englischer Artikel = " << wort << endl;
            return true;
        }
        return false;
    }
}

bool bestArt(string wort, int maxChars, string sprache) {
    if (wort.length() > maxChars) {
        cout << " Artikel " << wort << " ist zu lang" << endl;
        return false;
    } else
        return (bestArt(wort, sprache) || bestArt(wort, "en"));
}

int main() {
    string wort;
    cout << " Hello, Programm defaultValues.cpp " << endl;
    cout << " Bitte geben Sie einen bestimmten Artikel ein  >>";
    cin >> wort;
    if (bestArt(wort, 4))
        cout << " bestArt " << wort << " gefunden" << endl;
    else {
        cout << " bestArt " << wort << " nicht gefunden" << endl;
        return 1;
    }
}
