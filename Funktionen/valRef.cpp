// file: Funktionen/valRef.cpp
// description:

#include <iostream>
#include <string>
using namespace std;

// Definition der Prototypen
int addString(string);
int addString2(string &wort);

int addString(string wort) {
    wort.append("heit");
    cout << " In der Funktion wort = " << wort << endl;
    return 1;
}

int addString2(string &wort) {
    wort.append("heit");
    cout << " In der Funktion wort = " << wort << endl;
    return 1;
}

string addString3(string &wort) {
    wort.append("heit");
    cout << " In der Funktion wort = " << wort << endl;
    return wort;
}

int main() {
    string wort;
    string wort1,wort2,wort3;
    string neuesWort;
    cout << " Hello, Programm valRef.cpp " << endl;
    cout << " Bitte geben Sie ein wort ein  >>";
    cin >> wort;
    wort1=wort;
    wort2=wort;
    wort3=wort;
    addString(wort1);
    cout << " nach Aufruf von  addString=" << wort1 << endl;
    addString2(wort2);
    cout << " nach Aufruf von addString2=" << wort2 << endl;
    neuesWort = addString3(wort3);
    cout << " nach Aufruf von addString3=" << neuesWort;
    cout << ", Argument=" << wort3 << endl;
}
