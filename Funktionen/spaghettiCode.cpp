// file: Funktionen/spaghettiCode.cpp
#include <iostream>
using namespace std;

int main() {
    string s;
    int l,w,o;
    l = 0;
    w = 0;
    o = 0;
    getline(cin,s);
    for (int i=0 ; i < s.size(); i++) {
        if (s[i] > 96 && s[i] < 123 || s[i] > 64 && s[i] < 91) {
                l++;
        }
        else if (s[i] == 32) {
                w++;
        }
        else { 
                o++; 
        }
    }
    int x = 100.0 / (float) s.size();
    cout <<  s.size() << " : C " << l * x;
	cout << " ,W " << w * x << " ,O " << o * x << endl;
}
