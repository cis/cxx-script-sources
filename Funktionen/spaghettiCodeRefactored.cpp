#include <iostream>
#include <string>

using namespace std;

string readLineFromTerminal( void );
bool isLetter( char );
bool isWhitespace( char );
float ruleOfThree(int, int );
void printCharacterDistributionfor( string, int, int );
void debug(char);

int main() {
	string line;
	int letterCount, whitespaceCount, otherCharacterCount;

	whitespaceCount = 0;
	letterCount = 0;
	otherCharacterCount =0;

	line = readLineFromTerminal();

	for (int position = 0 ; position < line.size(); position++ ) {
		char currentCharacter = line.at( position );
		debug(currentCharacter);
		if ( isLetter( currentCharacter ) ) {
		letterCount++;
		}
		else if ( isWhitespace( currentCharacter ) ) {
		whitespaceCount++;
		cout << "WS"<< whitespaceCount << endl;
		}
		else { 
		otherCharacterCount++;
		}
	}
	printCharacterDistributionfor( "Letters",letterCount,line.size() );
	printCharacterDistributionfor( "Whitespace",whitespaceCount, line.size() );
	printCharacterDistributionfor( "Other Chars",otherCharacterCount, line.size() );
}

string readLineFromTerminal() {
	string s;
	getline(cin,s);
	return s;
}

bool isLetter(char c) {
	return c > 96 && c < 123 || c > 64 && c < 91 ;
}

bool isWhitespace(char c) {
	return  c == 32 ;
}

void printCharacterDistributionfor( string name, int count, int max) {
	cout <<  name << " " <<  ruleOfThree(max, count);
	cout << "\% of input." << endl;
}

float ruleOfThree( int all, int part) {
	float x = 100.0 /  (float) all;
	return x * part;
}

void debug(char currentCharacter) {
	cout << "Char:" << currentCharacter << " -> ";
	cout << (int) currentCharacter << endl;
}
