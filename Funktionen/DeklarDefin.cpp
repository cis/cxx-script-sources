// file: Funktionen/Deklar_Defin.cpp
#include <iostream>
using namespace std;

// Deklaration add mit  Prototyp:
int add(int arg1, int arg2 = 0);

// Deklaration add ohne  Prototyp:
int add2(int arg1, int arg2) {
  return arg1+arg2;
}

int main() {
  int erg;
  erg = add(1,3); // Aufruf mit 2 Argumenten
  cout << "add(1,3)=" << erg << endl;
  erg = add(1);   // Aufruf mit 1 Argument
  cout << "add(1)=" << erg << endl;
}
// Definition:
int add(int arg1, int arg2) {
  return arg1 + arg2;
}
