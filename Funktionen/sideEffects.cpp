// file: Funktionen/sideEffects.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

void addAndPrintStringByValue(string wort) {
    wort.append("heit");
    cout << " In der Funktion <addAndPrintString> wort = " << wort << endl;
}

void addAndPrintStringByReference(string &wort) {
    wort.append("heit");
    cout << " In der Funktion <addAndPrintString> wort = " << wort << endl;
}

int main() {
    string wort;

    cout << " Programm sideEffects.cpp " << endl;
    cout << " Bitte geben Sie ein wort ein  >>";
    cin >> wort;
    
    
    cout << "Funktion <addAndPrintStringByValue> wird nun drei mal aufgerufen" << endl;
    addAndPrintStringByValue(wort);
    addAndPrintStringByValue(wort);
    addAndPrintStringByValue(wort);
    
    cout << "Funktion <addAndPrintStringByRefernce> wird nun drei mal aufgerufen" << endl;
    addAndPrintStringByReference(wort);
    addAndPrintStringByReference(wort);
    addAndPrintStringByReference(wort);
}