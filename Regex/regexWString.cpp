// file: Regex/regexWString.cpp
// description: regex_match() mit Internationalisierung

#include <boost/regex.hpp>
#include <fstream>
#include <iostream>
#include <locale>
#include <string>

using namespace std;

int main(int argc, const char* argv[]) {
	
	setlocale(LC_CTYPE,"");
    locale mylocale("de_DE.utf-8");
    wstring zeile;
    wifstream datei;
    string filename;

    // Regulärer Ausdruck für wstring
    boost::wregex re1(L"(\\s*über\\s*)", boost::wregex::icase);
    boost::wregex re2(L"(\\s*\\w+\\s*)", boost::wregex::icase);
    boost::wsmatch what;

    wcout.imbue(mylocale); //  wcout mit dem locale 'einfärben'

    for (int i = 1; i < argc; ++i) {
        filename = argv[i];
        datei.open(filename.c_str());
        datei.imbue(mylocale);
        int j = 0;
        while (getline(datei, zeile)) {
            j++;
            //getline(datei,zeile);
            // Methode Regex_Match
            if (regex_match(zeile, what, re1)) {
                wcout << L"re1 found. Line= " << j << endl;
            }
            if (regex_match(zeile, what, re2)) {
                wcout << L"re2 found, Line= " << j << endl;
            }

        }
        datei.close();
    }
    return 0;
}

