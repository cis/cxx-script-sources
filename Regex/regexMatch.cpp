// file: Regex/regexMatch.cpp
// description:  regex_match()

#include <iostream>
#include <string>
#include <boost/foreach.hpp>
#include <boost/regex.hpp>

using namespace std;
using namespace boost;
int main() {        
    string namen[] = {"Leon","Lucas","Ben","Finn","Jonas"};
        
    // entspricht regex dreiBuchstaben("\\w\\w\\w" , boost::regex::perl)
    // alternative regex dreiBuchstaben("\\w\\w\\w");
    regex dreiBuchstaben("\\w{3}");
    //regex beginntMitL("^L"); w�rde nicht matchen, da submatch
    regex beginntMitL("^L.*");
        
        
    BOOST_FOREACH( string name, namen) {
                
        if ( regex_match(name,dreiBuchstaben ) ) {
            cout << "Name: " << name <<  " hat drei Buchstaben." << endl;
        }
        if ( regex_match(name,beginntMitL ) ) {
                cout << "Name: " << name <<  " beginnt mit L." << endl;
        }
    }
}
