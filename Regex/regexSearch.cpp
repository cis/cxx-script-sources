// file: Regex/regexSearch.cpp
// desription: regex_search(), 
//             Ausgabe aller W�rter einer Zeile

#include <fstream>
#include <iostream>
#include <fstream>
#include <locale>
#include <string>
#include <boost/regex.hpp>

using namespace std;
using namespace boost;

void search_in_wstring(wregex,wstring);

int main() {
	string file("eingabe.txt");
    setlocale(LC_ALL, "de_DE.UTF-8");
    
	wifstream utf_8_text(file.c_str());
    utf_8_text.imbue(locale("de_DE.UTF-8"));
	
	wstring utf_8_string;
    getline(utf_8_text, utf_8_string);
 
    wregex words(L"\\b(\\w+)\\b");
    wregex nonWhitespace(L"\\S+");
    wregex russianCharacters(L"???");
    
    
    search_in_wstring(words, utf_8_string);
    search_in_wstring(nonWhitespace, utf_8_string);
    search_in_wstring(russianCharacters, utf_8_string);
}

void search_in_wstring(wregex re, wstring line) {
    wsmatch aMatch;
        
    wstring::const_iterator startOfSearch = line.begin();
    wstring::const_iterator endOfSearch = line.end();
    //regex_search(Startposition, Endposition, MatchVariable, Regex)
    while ( regex_search(startOfSearch, endOfSearch, aMatch, re) ) {
            wcout << L"Matched: " << aMatch[0] << endl;
            startOfSearch = aMatch[0].second;
    }
    wcout << endl << endl;
}
