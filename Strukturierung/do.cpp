// file: Strukturierung/do.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int main() {
    string passwd = "eins";
    string gelesenPasswd;
    int zaehler = 0;

    do {
        cout << " Enter Password >>>";
        cin >> gelesenPasswd;
        zaehler++;
    } while (gelesenPasswd != passwd && zaehler < 5);

    if (gelesenPasswd == passwd)
        cout << " Passwort gefunden " << endl;
    else
        cout << " Passwort nicht gefunden " << endl;

    return 0;
}
