// file: Strukturierung/for.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int for2(void);
int for3(void);

int main() {
    cout << " Hallo! While loop " << endl;
    for2();
    for3();
    return 0;
}

int for2(void) {
    char a;
    cout << " Die Buchstaben von a bis g lauten: ";
    for (a = 'a'; a < 'g'; a++)
        cout << a;
    cout << endl;
}

int for3(void) {
    float x;
    cout << " Die Reihe von x - 1/x lautet: ";
    for (x = 3; x > 0.00005; x = x - 1 / x)
        cout << x << ", ";
    cout << endl;
    cout << " Eingabe der Obergrenze der for loop >>> ";
    int i, n;
    cin >> n;
    for (i = 0; i < n; i++)
        cout << " Schleifendurchlauf Nummer " << i << endl;
}
