// file: Strukturierung/while.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int while1(void);

int main() {
    cout << " Hallo! While loop " << endl;
    while1();
    return 0;
}

int while1(void) {
    int num;
    cout << "Gib den Countdown Wert ein >>>";
    cin >> num; /* Achtung vor Endlosschleifen */
    while (num > 0) {
        num--;
        cout << " Countdown laeuft >>" << num << endl;
    }
    cout << " Start " << endl;
    return 0;
}
