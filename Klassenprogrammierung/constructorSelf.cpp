// file: Klassenprogrammierung/constructorSelf.cpp 
// description: Implementation der Klasse lexikon

#ifndef CONSTRUCTORSELF_CPP
#define CONSTRUCTORSELF_CPP

#ifndef CONSTRUCTORSELF_HPP
#include "constructorSelf.hpp"
#endif

#include <iostream>
#include <string>

Lexikon::Lexikon() {
    land = "garkeins";
    umlaute = "";
};

Lexikon::Lexikon(const string &MyLand, const string &MyUmlaute) {
    land = MyLand;
    umlaute = MyUmlaute;
}


Lexikon::Lexikon(const string &MyLand) {
    land = MyLand;
};

void Lexikon::druckeEinstellungen() {
    cout << " Einstellungen = " << endl;
    cout << "Land = " << land << endl;
    cout << "Umlaute = " << umlaute << endl;
}

#endif
