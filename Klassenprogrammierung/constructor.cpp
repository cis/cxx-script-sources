// file: KLassenprogrammierung/constructor.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int main() {
    string wort1; // Defaultkonstruktor
    string wort2("wie gehts"); // weiterer Konstruktor

    cout << " Hello, Programm Konstruktor " << endl;
    cout << " Default Konstruktor: " << wort1 << endl;
    cout << " Default Konstruktor: " << wort2 << endl;
    return 0;
}
