// file: Klassenprogrammierung/mainSubstantiv.cpp
// description: Anwender der Substantiv Klasse

#include "substantiv.hpp"

#include <iostream>
#include <string>

using namespace std;

int main() {
    string wort;
    Substantiv subst;

    cout << " Hello, Programm mainSubstantiv.cpp " << endl;
    cout << " Genitiv eines Substantivs eingeben: ";
    cin >> wort;
    subst.intoGrundform(wort);
    subst.druckeLexikoneintrag();
    return 0;
}
