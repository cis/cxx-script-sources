// file: Klassenprogrammierung/strType.hpp
// description:

#include <iostream>

#ifndef STRTYPE_HPP
#define STRTYPE_HPP

class StrType {
public:
    // Konstruktoren:
    StrType(); // StrType s;
    StrType(const std::string); // StrType s1("string");
    StrType(const StrType &); // StrType s2(s1);

    // Destruktor:

    ~StrType() {
        anzChar = 0;
    }

    // Zuweisungsoperatoren:
    StrType operator=(const StrType); // s = s1;
    StrType operator=(const std::string); // s = "string";
    // Druckt die Anzahl der Buchstaben aus:
    int getAnzChars();

    friend std::ostream & operator<<(std::ostream&, const StrType&);


    // Verkn�pfungsoperator:
    StrType operator+(const StrType); // s1 + s2;
    StrType operator+(const std::string); // s1 + "string";
    // "string" + s1;
    friend StrType operator+(const std::string, const StrType);

private:
    std::string text;
    int anzChar;

};

StrType::StrType() {
    anzChar = 0;
    text = "";
}

// Konstruktor f�r Deklaration : StrType s1("string")

StrType::StrType(const std::string Str) {
    anzChar = Str.size();
    text = Str;
}

// Copy-Konstruktor f�r Deklaration : StrType s2(s1)

StrType::StrType(const StrType & Obj) {
    anzChar = Obj.anzChar; // Laenge von obj
    text = Obj.text; // kopiere obj.text des alten Objekts
}


// Ausgabeoperator f�r StrType:

std::ostream & operator<<(std::ostream &stream, const StrType &Obj) {
    stream << Obj.text;
    /* schicke text an den Stream
     * (obj.text kann er verarbeiten 
     * im Gegensatz zum ganzen Objekt) 
     */
    return stream; // Stream an  aufrufende Funktion zur�ck
}

// Zuweisung: s = s1;

StrType StrType::operator=(StrType Str) {
    text = Str.text;
    anzChar = text.size();
    return *this; // Objekt, an das zugewiesen wurde, zur�ck
}

// Zuweisung: s = "string";

StrType StrType::operator=(const std::string Str) {
    text = Str; // kopiere die Zeichenkette 
    anzChar = text.size();
    return *this; // Objekt, an das zugewiesen wurde, zur�ck
}

// Verkn�pfung: s1 + s2

StrType StrType::operator+(const StrType Str) {
    StrType tmp;

    tmp = text + Str.text;
    tmp.anzChar = tmp.text.size();
    return tmp; // gib verkn�pftes Objekt zur�ck
}

// Verkn�pfung: s1 + "string"

StrType StrType::operator+(const std::string Str) {
    StrType tmp;
    /* Tempor�res Objekt
     * (+ �ndert nicht die Werte seiner Operanden, 
     * -> zus�tzliches Objekt f�r Zuweisung n�tig)
     */
    tmp.anzChar = Str.size() + anzChar;
    tmp.text = Str + text;

    return tmp; // gibt verknuepftes Objekt zur�ck
}
// Verkn�pfung: "string" + s1

StrType operator+(const std::string Str, const StrType Obj) {
    StrType tmp;
    /* Tempor�res Objekt
     * (+ �ndert nicht die Werte seiner Operanden, 
     * -> zus�tzliches Objekt f�r Zuweisung n�tig) 
     */

    tmp.text = Str + Obj.text; //  text an 2. Operanden
    tmp.anzChar = tmp.text.size();

    return tmp; // gib verkn�pftes Objekt zur�ck
}
//Druckt die Anzahl der Buchstaben aus:

int StrType::getAnzChars() {
    return anzChar;
}

#endif
