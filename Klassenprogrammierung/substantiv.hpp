// file: Klassenprogrammierung/substantiv.hpp
// description:

#include <iostream>

#ifndef SUBSTANTIV_HPP
#define SUBSTANTIV_HPP

class Substantiv {
public:
    void intoGrundform(std::string &vollform);
    void druckeLexikoneintrag();
private:
    int genitiv(std::string &wort);
    std::string vollform;
    std::string grundform;
    std::string morphem;
};

void Substantiv::intoGrundform(std::string &wort) {
    vollform = wort;
    if (genitiv(wort))
        std::cout << " Genitiv entdeckt " << std::endl;
    else
        std::cout << " Endung nicht erkannt " << std::endl;
}

int Substantiv::genitiv(std::string &wort) {
    int lastChar = wort.length() - 1;
    if (wort[lastChar - 1] == 'e' && wort[lastChar] == 's') {
        morphem = "es";
        grundform.assign(wort, 0, lastChar - 1);
        return 1;
    }
    return 0;
}

void Substantiv::druckeLexikoneintrag() {
    if (grundform != "")
        std::cout << " Grundform = " << grundform << std::endl;
    if (morphem != "")
        std::cout << " Morphem = " << morphem << std::endl;
    if (vollform != "")
        std::cout << " Vollform = " << vollform << std::endl;
}
#endif
