// file: Klassenprogrammierung/constructorSelf.hpp 
// description: Deklarationsdatei f�r die Klasse lexikon

#ifndef CONSTRUCTORSELF_HPP
#define CONSTRUCTORSELF_HPP

#include <iostream>
#include <string>

class Lexikon {
public:
    void druckeEinstellungen();
    Lexikon();
    Lexikon(const std::string &MyLand, const std::string &MyUmlaute);
    Lexikon(const std::string &MyLand);

private:
    std::string land;
    std::string umlaute;
};

Lexikon::Lexikon() {
    land = "garkeins";
    umlaute = "";
};

Lexikon::Lexikon(const std::string &MyLand, const std::string &MyUmlaute) {
    land = MyLand;
    umlaute = MyUmlaute;
}


Lexikon::Lexikon(const std::string &MyLand) {
    land = MyLand;
};

void Lexikon::druckeEinstellungen() {
    std::cout << " Einstellungen = " << std::endl;
    std::cout << "Land = " << land << std::endl;
    std::cout << "Umlaute = " << umlaute << std::endl;
}

#endif
