// file: Klassenprogrammierung/mainConstructorSelf.cpp
// description: Anwender der Klasse lexikon

#include "constructorSelf.hpp"
#include <iostream>
#include <string>

using namespace std;

int main() {
    Lexikon meins;
    Lexikon englisch("englisch");
    Lexikon deutsch("deutsch", "�l�l�l");

    cout << " Hello, mainConstructorSelf " << endl;
    cout << " Konstruktor " << endl;
    meins.druckeEinstellungen();

    cout << " Konstruktor " << endl;
    englisch.druckeEinstellungen();
    cout << " Konstruktor " << endl;
    deutsch.druckeEinstellungen();
    return 0;
}
