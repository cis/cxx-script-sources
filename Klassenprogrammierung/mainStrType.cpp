// file: Klassenprogrammierung/mainStrType.cpp
// description:

#include "strType.hpp"
#include <iostream>

using namespace std;

int main() {
    StrType s1("Franz Hans Max"), s2(s1), s3, s4, s5, s6, s7;

    cout << "s1 = " << s1 << endl;
    cout << "s2 = " << s2 << endl;
    s3 = s1 + " " + s2;
    cout << "s3 = s1 + \" \" + s2: " << s3 << endl;
    s4 = s2;
    cout << "s4 = s2: " << s4 << endl;
    s5 = "Sepp";
    cout << "s5 :" << s5 << endl;

    s6 = "Das sind die Namen: " + s2;
    cout << "s6 :" << s6 << endl;
    cout << "Chars in s6 = " << s6.getAnzChars() << endl;

    return 0;
}
