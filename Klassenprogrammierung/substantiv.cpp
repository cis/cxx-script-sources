// file: Klassenprogrammierung/substantiv.cpp
// description:

#include "substantiv.hpp"
#include <iostream>
#include <string>

using namespace std;

void Substantiv::intoGrundform(string &wort) {
    vollform = wort;
    if (genitiv(wort))
        cout << " Genitiv entdeckt " << endl;
    else
        cout << " Endung nicht erkannt " << endl;
}

int Substantiv::genitiv(string &wort) {
    int lastChar = wort.length() - 1;
    if (wort[lastChar - 1] == 'e' && wort[lastChar] == 's') {
        morphem = "es";
        grundform.assign(wort, 0, lastChar - 1);
        return 1;
    }
    return 0;
}

void Substantiv::druckeLexikoneintrag() {
    if (grundform != "")
        cout << " Grundform = " << grundform << endl;
    if (morphem != "")
        cout << " Morphem = " << morphem << endl;
    if (vollform != "")
        cout << " Vollform = " << vollform << endl;
}