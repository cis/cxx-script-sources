// file: Templates/templates.cpp
// description: work with Templates  

#include <iostream>
#include <string>

using namespace std;

template <class X> void my_swap(X &a, X &b) {
    X temp;
    temp = a;
    a = b;
    b = temp;
}

main() {
    int i = 10, j = 20;
    float x = 10.1, y = 23.3;
    string one = "first";
    string two = "second";

    cout << " Hier ist das Programm templates " << endl;
    cout << " Before i=" << i << " j=" << j << endl;
    my_swap(i, j); //swap integer
    cout << " After i=" << i << " j=" << j << endl;

    cout << " Before x=" << x << " y=" << y << endl;
    my_swap(x, y); //swap floats
    cout << " After x=" << x << " y=" << y << endl;

    cout << " Before one=" << one << " two=" << two << endl;
    my_swap(one, two); // swap strings
    cout << " After one=" << one << " two=" << two << endl;

}
