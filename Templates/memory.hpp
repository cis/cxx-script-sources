// file: Templates/memory.hpp
// description: work with Templates  

#ifndef MEMORY_HPP
#define MEMORY_HPP

#include <iostream>
#include <string>

const int MaxAnz = 10;

template <class X>
class memory {
public:
    bool storeElement(X &element);
    bool findElement(X &element);

    memory() {
        numOfElements = 0;
    };
private:
    X allElements[MaxAnz];
    int numOfElements;
};

template<class X>
bool memory<X>::storeElement(X &element) {
    std::cout << " Store " << element << std::endl;
    if (numOfElements < MaxAnz) {
        allElements[numOfElements] = element;
        numOfElements++;
        std::cout << " Success " << std::endl;
        return true;
    } else {
        std::cout << " Memory is full " << std::endl;
        return false;
    }
}

template<class X>
bool memory<X>::findElement(X &element) {
    int index = 0;
    std::cout << " find " << element << std::endl;
    while (index < numOfElements &&
            allElements[index] != element) {
        index++;
    }
    if (allElements[index] == element) {
        std::cout << " Found " << std::endl;
        return true;
    } else {
        std::cout << " Not Found " << std::endl;
        return false;
    }
}

#endif
