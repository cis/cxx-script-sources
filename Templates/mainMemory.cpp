// file: Templates/mainMemory.cpp
// description:

#include <iostream>
#include <string>

// Einbinden der Header Datei:
#include "memory.hpp"

using namespace std;

int main() {
    string wort;
    memory<string> stringMemory;

    cout << " Programm mainMemory.cpp " << endl;
    do {
        cout << " Bitte geben Sie ein Wort ein: ";
        cin >> wort;
        stringMemory.storeElement(wort);
    } while (stringMemory.findElement(wort));

    return 0;
}
