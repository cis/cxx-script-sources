// file: Templates/memory.cpp
// description: work with Templates  

#include <iostream>
#include <string>

using namespace std;

template<class X>
bool memory<X>::storeElement(X &element) {
    cout << " Store " << element << endl;
    if (numOfElements < MaxAnz) {
        allElements[numOfElements] = element;
        numOfElements++;
        cout << " Success " << endl;
        return true;
    } else {
        cout << " Memory is full " << endl;
        return false;
    }
}

template<class X>
bool memory<X>::findElement(X &element) {
    int index = 0;
    cout << " find " << element << endl;
    while (index < numOfElements &&
            allElements[index] != element) {
        index++;
    }
    if (allElements[index] == element) {
        cout << " Found " << endl;
        return true;
    } else {
        cout << " Not Found " << endl;
        return false;
    }
}
