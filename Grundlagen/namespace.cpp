// file: Grundlagen/namespace.cpp
// description: work with namespaces

#include <iostream>
#include <string>

using namespace std;
string programname = "namespace.cpp";

namespace A {
    string name; // A::name
}

int main() {

    string name;
    cout << " Hallo! Das Programm " << programname << endl;
    cout << " Bitte geben Sie einen String ein: ";
    cin >> A::name;
    cout << " Der String ist " << A::name << endl;
    cout << " Bitte geben Sie einen zweiten String ein: ";
    cin >> name;
    cout << A::name << " und " << name << endl;
    return 0;
}
