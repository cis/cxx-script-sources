// file: Grundlagen/eins.cpp
// description:

#include <iostream>    // systemweite Header
#include <string>

using namespace std;

// Prototypen: Funktionen werden deklariert:
int begruessung();
int verabschiedung();

int main() {
    const string Space(5, ' ');
    string vorname, nachname;
    string name;

    begruessung();
    cout << " Bitte Vornamen eingeben: ";
    cin >> vorname;
    cout << " Bitte Nachnamen ein: ";
    cin >> nachname;
    name = vorname + Space + nachname;
    cout << "Ihr Name ist " << name << endl;
    verabschiedung();
    return 0;
}

int begruessung() { // Funktionskopf
    cout << "Guten Tag!" << endl; // Funktionsrumpf
    return 0;
}

int verabschiedung() {
    cout << "Auf Wiedersehen" << endl;
    return 0;
}
