// file: Grundlagen/namespace2.cpp
// description:

#include <iostream>
#include <string>

using namespace std;
string programName = "namespace2.cpp";

namespace A {
    string name; // A::name
}

int main() {
    string name;
    cout << " Das Programm " << programName << " << endl ";
    cout << " Bitte geben Sie einen String ein: ";
    cin >> A::name;
    cout << " Der String ist " << A::name << endl;
    cout << " Bitte geben Sie einen zweiten String ein: ";
    cin >> name;
    cout << A::name << " und " << name << endl;
    return 0;
}
