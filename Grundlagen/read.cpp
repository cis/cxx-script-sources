// file: Grundlagen/read.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int main() {

    char zeichen;
    while (cin.get(zeichen))
        cout.put(zeichen);

    return 0;
}
