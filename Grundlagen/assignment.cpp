// file: Grundlagen/assignment.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int main() {
    string str1("Hallo"), str2 = "wie gehts", str3;
    int val1 = 100, val2 = 10;
    float pi = 3.14;

    cout << "Der Wert von str1 = " << str1 << endl;

    str3 = str1;
    // genug Speicher f�r  str3 alloziert
    // jeder Buchstabe von str1 in str3 kopiert.

    cout << " Der Wert von str3 = " << str3 << endl;

    cout << " Der Wert von val1 = " << val1 << endl;
    val1 += val2;
    cout << " Nach val += val2; val1 = " << val1 << endl;

    int intPi = pi; // implizite Konvertierung
    cout << " pi : " << pi << " intPi = " << intPi << endl;

    return 0;

}
