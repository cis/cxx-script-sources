// file: Grundlagen/fileIo.cpp
// description:

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    string filenameIn, filenameOut;
    string zeile;
    ifstream textfileIn("eingabe.txt");
    // oder: ifstream textfileIn(filenameIn.c_str() );
    ofstream textfileOut("ausgabe.txt");

    if (!textfileIn) //siehe Status-Flags
    {
        cout << " Fileopen Error " << endl;
        return(-1);
    }

    while (getline(textfileIn, zeile)) {
        cout << " Die Zeile war: " << zeile << endl;
        textfileOut << " Die Zeile war: " << zeile << endl;
    }

    return 0;
}
