// file: Grundlagen/lexComp.cpp
// description:

#include <iostream>
#include <string>

using namespace std;

int main() {
    const string Ref = "Hallo";
    string eingabe;

    cout << " Bitte geben Sie einen String ein >>>";
    cin >> eingabe;

    if (eingabe > Ref)
        cout << eingabe << " groesser als " << Ref << endl;

    if (eingabe == Ref)
        cout << eingabe << " identisch zu " << Ref << endl;

    if (eingabe < Ref)
        cout << eingabe << " kleiner als " << Ref << endl;

    return 0;

}
