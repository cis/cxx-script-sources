// file: Grundlagen/io.cpp
// description:

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    string filenameIn, filenameOut;
    string zeile;
    ifstream textfileIn;
    ofstream textfileOut;

    cout << " Name der Eingabedatei: ";
    cin >> filenameIn;
    cout << " Name der Ausgabedatei ein: ";
    cin >> filenameOut;

    textfileIn.open(filenameIn.c_str());
    textfileOut.open(filenameOut.c_str());

    if (!textfileIn) //siehe Status-Flags
    {
        cout << " Fileopen Error on " << filenameIn << endl;
        return(-1);
    }
    while (getline(textfileIn, zeile)) {
        cout << " Die Zeile war: #" << zeile << "#" << endl;
        textfileOut << " Die Zeile war: #" << zeile << "#" << endl;
    }
    return 0;
}
