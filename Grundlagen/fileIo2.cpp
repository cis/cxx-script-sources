// file: Grundlagen/fileIo2.cpp
// description:

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
    string filenameIn, filenameOut;
    string zeile;

    cout << " Name der Eingabedatei: ";
    cin >> filenameIn;
    cout << " Name der Ausgabedatei: ";
    cin >> filenameOut;

    ifstream textfileIn("eingabe.txt");
    ofstream textfileOut("ausgabe.txt");

    if (!textfileIn) {
        cout << " Fileopen Error " << endl;
        return(-1);
    }

    while (getline(textfileIn, zeile)) {
        cout << " Die Zeile war: " << zeile << endl;
        textfileOut << " Die Zeile war: " << zeile << endl;
    }

    return 0;
}
