#
#  Erstellt am: 22.07.2012
#       Author: Stefan Schweter
# Beschreibung: Makefile für das C++ Skript
#

CC=g++

# Kompatibilitätsbereich GCC [4.3 - 4.7[
CPP11=-std=c++0x

REV=$$(git rev-parse HEAD | head -c 10)

all: Grundlagen Strukturierung Stringeinsatz Funktionen\
 Internationalisierung Klassenprogrammierung Vererbung Templates STL\
 Regex Ausnahme C11

dist:
	@git archive --format=tar.gz --prefix=cxx-script-sources/ \
	HEAD > cxx-script-sources_$(REV).tar.gz

Grundlagen: eins namespace namespace2 assignment read io fileIo fileIo2\
 lexComp

Strukturierung: while do for

Stringeinsatz: stringLexComp stringComp stringInit searchNpos substring\
 sideEffects

Funktionen: spaghettiCode spaghettiCodeRefactored DeklarDefin\
 defaultValues valRef sideEffects

Internationalisierung: locale WcharTests convToISO printUTF8File

Klassenprogrammierung: mainSubstantiv mainConstructorSelf mainStrType

Vererbung: mainEsel tierVirtMain mainIndex

Templates: mainMemory

STL: makeP vector vectorIt vectorRit list set mapCount unordered_set\
 unordered_map dequeConc find_last_of find_last_of2 search search_n\
  count_if hash_mapUTF8

Regex: regexMatch regexSearch regexWString

Ausnahme: Exception BigException

C11: container pair auto container2 containerold fakultaet range-for\
 iterator-range-for all_of any_of none_of iota benchmark benchmarkChrono

# Grundlagen
eins:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp

namespace:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp

namespace2:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp

assignment:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp

read:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp

io:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp

fileIo:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp

fileIo2:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp

lexComp:
	${CC} -o Grundlagen/$@ Grundlagen/$@.cpp


# Strukturierung
while:
	${CC} -o Strukturierung/$@ Strukturierung/$@.cpp

do:
	${CC} -o Strukturierung/$@ Strukturierung/$@.cpp

for:
	${CC} -o Strukturierung/$@ Strukturierung/$@.cpp


# Stringeinsatz
stringLexComp:
	${CC} -o Stringeinsatz/$@ Stringeinsatz/$@.cpp

stringComp:
	${CC} -o Stringeinsatz/$@ Stringeinsatz/$@.cpp

stringInit:
	${CC} -o Stringeinsatz/$@ Stringeinsatz/$@.cpp

searchNpos:
	${CC} -o Stringeinsatz/$@ Stringeinsatz/$@.cpp

substring:
	${CC} -o Stringeinsatz/$@ Stringeinsatz/$@.cpp


# Funktionen
spaghettiCode:
	${CC} -o Funktionen/$@ Funktionen/$@.cpp

spaghettiCodeRefactored:
	${CC} -o Funktionen/$@ Funktionen/$@.cpp

DeklarDefin:
	${CC} -o Funktionen/$@ Funktionen/$@.cpp

defaultValues:
	${CC} -o Funktionen/$@ Funktionen/$@.cpp

valRef:
	${CC} -o Funktionen/$@ Funktionen/$@.cpp

sideEffects:
	${CC} -o Funktionen/$@ Funktionen/$@.cpp


# Internationalisierung
locale:
	${CC} -o Internationalisierung/$@ Internationalisierung/$@.cpp

convToISO:
	${CC} -o Internationalisierung/$@ Internationalisierung/$@.cpp

WcharTests:
	${CC} -o Internationalisierung/$@ Internationalisierung/$@.cpp

printUTF8File:
	${CC} -o Internationalisierung/$@ Internationalisierung/$@.cpp

# Klassenprogrammierung
mainSubstantiv:
	${CC} -o Klassenprogrammierung/$@ Klassenprogrammierung/$@.cpp

mainConstructorSelf:
	${CC} -o Klassenprogrammierung/$@ Klassenprogrammierung/$@.cpp

mainStrType:
	${CC} -o Klassenprogrammierung/$@ Klassenprogrammierung/$@.cpp


# Vererbung
mainEsel:
	${CC} -o Vererbung/$@   Vererbung/$@.cpp

tierVirtMain:
	${CC} -o Vererbung/$@   Vererbung/$@.cpp

mainIndex:
	${CC} -o Vererbung/$@   Vererbung/$@.cpp


# Templates
mainMemory:
	${CC} -o Templates/$@   Templates/$@.cpp


# STL
makeP:
	${CC} -o STL/$@ STL/$@.cpp

vector:
	${CC} -o STL/$@ STL/$@.cpp

vectorIt:
	${CC} -o STL/$@ STL/$@.cpp

vectorRit:
	${CC} -o STL/$@ STL/$@.cpp

list:
	${CC}  -o STL/$@ STL/$@.cpp

set:
	${CC} -o STL/$@ STL/$@.cpp

mapCount:
	${CC}  -o STL/$@ STL/$@.cpp

unordered_set:
	${CC} ${CPP11} -o STL/$@   STL/$@.cpp

unordered_map:
	${CC} ${CPP11} -o STL/$@   STL/$@.cpp

dequeConc:
	${CC} -o STL/$@ STL/$@.cpp

find_last_of:
	${CC} -o STL/$@ STL/$@.cpp

find_last_of2:
	${CC} -o STL/$@ STL/$@.cpp

search:
	${CC} -o STL/$@ STL/$@.cpp

search_n:
	${CC} -o STL/$@ STL/$@.cpp

count_if:
	${CC} -o STL/$@ STL/$@.cpp

hash_mapUTF8:
	${CC} -Wno-deprecated -o STL/$@ STL/$@.cpp


# Regex
regexMatch:
	${CC} -o Regex/$@ Regex/$@.cpp -lboost_regex

regexSearch:
	${CC} -o Regex/$@ Regex/$@.cpp -lboost_regex

regexWString:
	${CC} -o Regex/$@ Regex/$@.cpp -lboost_regex


# Ausnahme
Exception:
	${CC} -o Ausnahme/$@ Ausnahme/$@.cpp

BigException:
	${CC} -o Ausnahme/$@ Ausnahme/$@.cpp


# C11
container:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

pair:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

auto:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

container2:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

containerold:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

fakultaet:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

range-for:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

iterator-range-for:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

all_of:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

any_of:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

none_of:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

iota:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

benchmark:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp

benchmarkChrono:
	${CC} ${CPP11} -o c11/$@   c11/$@.cpp
