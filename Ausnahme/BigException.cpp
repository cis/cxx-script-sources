// file: Exception/bigException.cpp
// description:

#include <iostream>
#include <fstream>
#include <locale>
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <string>

using namespace std;

int small_exception();
int read_from_file(wifstream &wfile);
int read_from_file_with_exception(wifstream &wfile);

int main() {
    wstring line;
    locale mylocale("de_DE.utf-8");
    locale::global(mylocale);
    wifstream wfile("in.txt");
    wfile.imbue(mylocale);
    int result;
    bool test1 = false;
    bool test2 = false;
    bool test3 = true;


    if (test1) {
        result = small_exception();
        wcout << "Small Exception: " << result << endl;
        exit(0);
    }

    wcout << "We set File 'in.txt' to utf8 " << endl;
    if (wfile.fail()) {
        wcout << "ERROR: open wfile " << endl;
        exit(0);
    }

    if (test2) {
        result = read_from_file(wfile);
        wcout << "read_from_file: " << result << endl;
        exit(0);
    }

    if (test3) {
        result = read_from_file_with_exception(wfile);
        wcout << "read_from_file_with_exception: " <<
                result << endl;
        exit(0);
    }

}

int read_from_file_with_exception(wifstream &wfile) {
    //  wfile.exceptions(ios::eofbit|ios::failbit|ios::badbit);
    //Definiert Standard Exceptions.

    class Lesefehler : public exception {
    };
    wstring line;
    ios::iostate status;

    do {
        try {
            getline(wfile, line);
            if (wfile.fail()) throw Lesefehler();
            if (wfile.eof()) throw Lesefehler();
            if (wfile.bad()) throw Lesefehler();
            wcout << "--->IN>>" << line << endl;
        } catch (const Lesefehler& Error) {
            wcout << L"Lesefehler Error" << endl;
            wcout << "error.what(): " << Error.what() << endl;
            wcout << "strerror:" << errno << "=" << strerror(errno) << endl;
            if (wfile.fail()) {
                wcout << "Fail situation, we try to clear it" << endl;
                wfile.clear();
                wcout << "test eof Bit" << endl;
                if (wfile.eof()) {
                    wcout << "Still eof Bit is set" << endl;
                } else {
                    wcout << "eof bit cleared, skip 1 byte" << endl;
                    wfile.clear();
                    wfile.seekg(3, ios::cur);
                    wfile.clear();
                }
            }
        } catch (const exception& Error) {
            wcout << "Standard Exception" << endl;
            wcout << "error.what(): " << Error.what() << endl;
        } catch (const char *Code) {
            wcout << "Default Exception CODE" << endl;
        } catch (...) {
            wcout << "Default Exception" << endl;
        };

        wcout << "--->IN: #" << line << "#" << endl;
        wcout << "eof() bit " << wfile.eof() << endl;
        wcout << "fail() bit " << wfile.fail() << endl;
        wcout << "bad() bit " << wfile.bad() << endl;
        wcout << "strerror: " << strerror(errno) << endl;

        status = wfile.rdstate();
        wcout << L" Status des Lesens: " << status << endl;

        /*     if (!status)
{
  wfile.setstate(ios::goodbit);
  wcout << "IN: #" << line << "#" << endl;
}
         */
    } while (!wfile.eof());

    return status;
}

int small_exception() {

    class Fehler : public exception {
    };

    try {
        throw Fehler();
        throw 666; // int, wird abgefangen
        // wird nicht mehr ausgeführt:
        throw L"stone";
        wcout << L"kein Fehler aufgetreten!\n";
    } catch (int i) {
        wcout << L"int-Fehler " << i << " abgefangen\n";
    } catch (wstring str) {
        wcout << L"stringfehler " << str << " abgefangen\n";
    } catch (Fehler &f) {
        wcout << L"Fehler, eine Klasse" << endl;
        wcout << "was ist los : " << f.what() << endl;
    }
    return EXIT_SUCCESS;
}

int read_from_file(wifstream &wfile) {
    wstring line;
    while (getline(wfile, line)) {
        wcout << "--->IN>>" << line << endl;
    };
    wcout << "Error strerror :=" << strerror(errno) << endl;
    return EXIT_SUCCESS;
}
