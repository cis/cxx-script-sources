// file: Exception/exception.cpp
// description:

#include <iostream>
#include<string>

using namespace std;

int main() {

    class Fehler : public exception {
    };

    locale mylocale("de_DE.utf-8");
    locale::global(mylocale);

    try {
        throw Fehler();
        throw 666; // int, wird abgefangen
        // wird nicht mehr ausgeführt:
        throw L"stone";
        wcout << L"kein Fehler aufgetreten!\n";
    } catch (int i) {
        wcout << L"int-Fehler " << i << " abgefangen\n";
    } catch (wstring str) {
        wcout << L"Stringfehler " << str << " abgefangen\n";
    } catch (Fehler &f) {
        wcout << L"Fehler, eine Klasse" << endl;
        wcout << "was ist los : " << f.what() << endl;
    }
    return 0;
}
