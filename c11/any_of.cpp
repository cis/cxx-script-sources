// file: c11/any_of.cpp
// description:

#include <algorithm>
#include <vector>
#include <iostream>

using namespace std;

int main() {
	vector<int> intVector = { 1, 2, 3, 5, 6, 0 };
	if(any_of(intVector.begin(), intVector.end(), [](int a){return a>5;})) {
		cout << "Mindestens eine Zahl ist gr��er als 5" << endl;
	}
	else {
		cout << "Leider ist keine Zahl gr��er als 5!" << endl;
	}
}
