// file: c11/auto.hpp
// description:

using namespace std;

int main() {
	auto i = 0; // i ist ist vom Typ integer
	auto d = 1.337; // d ist vom Typ double
	auto c = 'c'; // c ist vom Typ char
	
	int irgendein_int;
	decltype(irgendein_int) andere_int = 5; 
	// somit ist andere_int vom Typ integer
}
