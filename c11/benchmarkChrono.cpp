// file: c11/benchmarkChrono.hpp
// description:

#include <chrono>
#include <locale>
#include <fstream>
#include <iostream>
#include <tr1/unordered_map>
#include <boost/foreach.hpp>

int main() {

	setlocale(LC_ALL, "");
	
	std::wstring line;
	
	std::locale de_utf8("de_DE.utf8"); // Sollte bitte vorhanden sein!
	std::wifstream inputFileStream("out.txt");
	inputFileStream.imbue(de_utf8);
	
	std::tr1::unordered_map<std::wstring, long> normal_map;
	
	auto startReading = std::chrono::system_clock::now();

	// Fire, fire!!!
	while(getline(inputFileStream, line)) {
		normal_map[line]++;
	}
	
	auto endReading = std::chrono::system_clock::now();
	
	auto readingDuration = endReading - startReading;
	
	std::chrono::seconds readingDurationInSeconds(std::chrono::duration_cast<std::chrono::seconds>(readingDuration));
	
	std::cout << "Das Einlesen der Datei dauerte ";
	std::cout << readingDurationInSeconds.count();
	std::cout << " Sekunden" << std::endl;
	
	auto startIteration = std::chrono::system_clock::now();
	
	auto counter = 0;
	
	/*
	for (auto it = normal_map.begin(); it != normal_map.end(); ++it) {
		counter++;
	}
	*/
	
	
	/*
	for (const auto x : normal_map) {
		counter++;
	}
	*/
	
	
	BOOST_FOREACH(const auto x, normal_map ) {
		counter++;
    }
    
    
	auto endIteration = std::chrono::system_clock::now();
	
	auto iterationDuration = endIteration - startIteration;
	
	std::chrono::seconds iterationDurationInSeconds(std::chrono::duration_cast<std::chrono::seconds>(iterationDuration));
	
	//std::cout << "Die Iteration mittels For- Schleife dauert: ";
	//std::cout << "Die Iteration mittels Range-For-Schleife dauert: ";
	std::cout << "Die Iteration mittels BOOST-For-Each dauert: ";
	
	std::cout << iterationDurationInSeconds.count();
	std::cout << " Sekunden" << std::endl;
	
	std::cout << "Iterationsdurchläufe: " << counter << std::endl;
	
	return 0;
}
