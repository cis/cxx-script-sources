// file: c11/range-for.hpp
// description:

#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> vekki) {
	for(auto element : vekki) {
		cout << element << endl;
	}
}

void printZahlen() {
	for(int n : { 1,4,7,8,9,3333,455435,5423,232,1337 }) {
		cout << n << endl;
	}
}

int main() {
	vector<int> test = { 1,2,3,4 };
	printVector(test);
	printZahlen();
}
