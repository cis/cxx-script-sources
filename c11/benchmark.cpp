#include <locale>
#include <tr1/unordered_map>
#include <fstream>
#include <iostream>

struct jenkins_hash {
	
	size_t operator()(const std::wstring &word) const {
		size_t h = 0;
		for(size_t i = 0; i < word.length(); i++) {
			h += word[i];
			h += (h << 10);
			h ^= (h >> 6);
		}
		h += (h << 3);
		h ^= (h >> 11);
		h += (h << 15);
		return h;
	}
};

struct bad_hash {
	
	size_t operator()(const std::wstring &word) const {
		return word.length();
	}
};

struct verybad_hash {
	size_t operator()(const std::wstring &word) const {
		return 0;
	}
};

struct stefan_hash {
	size_t operator()(const std::wstring &word) const {
		size_t h = 0;
		for(size_t i = 0; i < word.length(); i++) {
			h += i * (unsigned char)word[i];
		}
		return h;
	}
};

int main() {

	setlocale(LC_ALL, "");

	std::wstring line;
	
	std::locale de_utf8("de_DE.utf8"); // Sollte bitte vorhanden sein!
	std::wifstream inputFileStream("wordlist.txt");
	inputFileStream.imbue(de_utf8);
	
	// Definiere die Container f�r den Benchmark
	//std::tr1::unordered_map<std::wstring, long, jenkins_hash> jenkins_map;
	//std::tr1::unordered_map<std::wstring, long> normal_map;
	//std::tr1::unordered_map<std::wstring, long, stefan_hash> stefan__map;
	//std::tr1::unordered_map<std::wstring, long, bad_hash> bad_map;
	//std::tr1::unordered_map<std::wstring, long, verybad_hash> verybad_map;

	// Fire, fire!!!
	while(getline(inputFileStream, line)) {
		//jenkins_map[line]++;
		//normal_map[line]++;
		//stefan__map[line]++;
		//bad_map[line]++;
		//verybad_map[line]++;
	}
	return 0;
}
