// file: c11/container.cpp
// description:

#include <vector>
#include <map>
#include <string>
#include <iostream>

using namespace std;

int main() {
	
	vector<string> stringVector = { "Wert", "Neuer Wert", "Ganz neuer Wert"};
	vector<int> intVector = { 1, 2, 3, 5, 6 };
	map<int,string> listVector = { {1, "Max"}, {2, "Andi"}, {3, "Stefan"} };
	
	// Ausgabe
	map<int,string>::iterator it;
	
	for(it = listVector.begin(); it != listVector.end(); ++it) {
		cout << it->first << " " << it->second << endl;
	}
}
