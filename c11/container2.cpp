// file: c11/container2.hpp
// description:

#include <vector>
#include <map>
#include <string>
#include <iostream>

using namespace std;

int main() {
	
	map<int,string> listVector = { {1, "Max"}, {2, "Andi"}, {3, "Stefan"} };
	
	// Ausgabe
	for (auto it = listVector.begin(); it != listVector.end(); ++it) {
		cout << it->first << " " << it->second << endl;
	}
}
