// file: c11/all_of.cpp
// description:

#include <algorithm>
#include <vector>
#include <iostream>

using namespace std;

int main() {
	vector<int> intVector = { 1, 2, 3, 5, -6 };
	if(all_of(intVector.begin(), intVector.end(), [](int a){return a >=0;})) {
		cout << "Alle Zahlen sind positiv!" << endl;
	}
	else {
		cout << "Leider sind nicht alle Zahlen positiv!" << endl;
	}
}
