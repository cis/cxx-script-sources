// file: c11/iota.cpp
// description:

#include <algorithm>
#include <numeric>
#include <iostream>

using namespace std;

int main() {
	int a[5] = {0}; // initialisiertes Array
	
	// Startwert soll 42 sein
	iota(a, a+5, 42);
	
	// Das ganze machen wir jetzt mal mit Buchstaben 
	// Um das komplette ABC zu erzeugen!
	char c[26] = {0};
	iota(c,c+26, 'a');
	
	// Gebe uns die Werte mal aus! Range- based loop in c++11
	for (int &i : a) {
		cout << i << endl;
	}
	
	// Werte des c- Arrays:
	for (char &c2 : c) {
		cout << c2 << endl;
	}
}
