#include <iostream>
#include <string>
#include <utility>

std::pair<std::string, int> func(std::string name, int val) {
		return { name, val*23};
	}

int main() {
	
	// Deklaration + Initialisierung
	std::pair<std::string, int> test{func("Hallo", 13)};
	
	// Und wie greife ich jetzt auf die beiden R�ckgaben zu....
	std::cout << test.first << " " << test.second << std::endl; // So ;)
	
	return 0; 
}
