// file: c11/none_of.cpp
// description:

#include <algorithm>
#include <vector>
#include <iostream>

using namespace std;

int main() {
	vector<int> intVector = { 1, 2, 3, 5, -6 };
	if(none_of(intVector.begin(), intVector.end(), [](int a){return a <0;})) {
		cout << "Keine Zahl ist kleiner als Null" << endl;
	}
	else {
		cout << "Leider sind nicht alle Zahlen gr��er als Null!" << endl;
	}
}
