// file: c11/fakultaet.hpp
// description:

#include <iostream>

auto fakultaet(int n) -> int {
	if (n <= 1) {
		return 1;
	}
	else {
		return n*fakultaet(n-1);
	}
	// K�rzer auch mit tern�ren Operator -> �bung ;)
}

int main() {
	std::cout << fakultaet(5) << std::endl;
}
