// file: c11/iterator-range-for.hpp
// description:

#include <iostream>
#include <vector>
#include <string>
#include <map>


using namespace std;

typedef map<wstring, int> frequencyMap;
frequencyMap::iterator it;

void printFrequencyIt(frequencyMap &frq) {
	for(auto iter = frq.begin(); iter != frq.end(); ++iter) {
		wcout << iter->first << L" kommt "; 
		wcout << iter->second << L" Mal vor!" << endl;
	}
}

void printFrequencyRange(frequencyMap &frq) {
	for(auto word : frq) {
		wcout << word.first << L" kommt ";
		wcout << word.second << L" Mal vor!" << endl;
	}
}

int main() {
	// Beachte die wide- strings!!!
	frequencyMap frq = { {L"Max", 5}, {L"Andi", 4}, {L"Stefan", 1} }; 

	printFrequencyIt(frq);
	wcout << endl;
	printFrequencyRange(frq);

	return 0;
}
