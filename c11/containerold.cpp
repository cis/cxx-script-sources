// file: c11/containerold.hpp
// description:

#include <vector>
#include <map>
#include <string>
#include <iostream>

using namespace std;

int main() {
	
	map<int,string> listVector;
	map<int,string>::iterator it;
	listVector.insert(map<int,string>::value_type(1, "Max"));
	listVector.insert(map<int,string>::value_type(2, "Andi"));
	listVector.insert(map<int,string>::value_type(3, "Stefan"));
	
	for(it = listVector.begin(); it != listVector.end(); ++it) {
		cout << it->first << " " << it->second << endl;
	}
}
