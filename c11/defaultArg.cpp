#include <iostream>
#include <string>
#include <vector>

// Deklaration, Prototyp
void tuEtwas(int irgendwas, std::string name = "Hallo" );

int main() {
	
	tuEtwas(2); // Weil Prototyp definiert wurde!

	return 0;
}

void tuEtwas(int irgendwas, std::string name) {
	
	std::cout << name << std::endl;
	std::cout << irgendwas << std::endl;
}
