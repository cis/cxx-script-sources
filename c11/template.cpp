#include <iostream>

template<typename A, typename B>
auto add(A a, B b) -> decltype(a+b) {
	return a+b;
}

int main() {
	
	// Testcases:
	std::cout << add(1,2) << std::endl;
	std::cout << add(1.2,2.8) << std::endl;
	std::cout << add(1, 3.6) << std::endl;
}
