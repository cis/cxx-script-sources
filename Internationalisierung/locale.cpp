// file: Internationalisation/locale.cpp
// description:

#include <iostream>
#include <string>
#include <locale>

using namespace std;

int main() {
    unsigned char line[256];
    int result;
    wstring name = L"Müller Hans";
    wstring farbe(L"grün");
    wstring fullname, str;

	setlocale(LC_ALL, "");

    wcout << L"Der Name lautet " << name << endl;
    wcout << L"Die Farbe ist " << farbe << endl;

    wcout << "Vor- und Nachnahmen eingeben: ";
    getline(wcin, fullname);
    wcout << " Ihr Name ist " << fullname << endl;


    // oder als Schleife,

    while (wcout << "Eingabe :" << flush, getline(wcin, str)) {
        wcout << str << endl;
    }
    wcout << endl;

    return 0;
}
