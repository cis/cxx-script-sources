// file: Internationalisation/convToISO.cpp
// description:

#include <iostream>
#include <locale>
#include <fstream>

using namespace std;

int main() {

	setlocale(LC_ALL, "");

    string filenameIso = "iso.txt";
    wstring isoLine;
    int i;
    wifstream fIso;

    // Construct locale object
    locale isolocale("de_DE.ISO-8859-1");
    locale utf8locale("de_DE.UTF-8");

    wcout << L"Open ISOlatin File" << endl;

    fIso.open(filenameIso.c_str());
    fIso.imbue(isolocale);

    if (!fIso) {
        wcerr << "Error opening " << endl;
        return 1;
    }

    i = 0;
    while (getline(fIso, isoLine))
        // iso: getline(<ifstream>,<string>)
    {
        // wcout mit <string>
        wcout << L"[" << i << L"]  " << isoLine << endl;
        i++;
    }
    
    fIso.close();

    return 0;

}
