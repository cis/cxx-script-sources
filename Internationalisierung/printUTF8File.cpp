// file: Internationalisation/printUTF8File.cpp
// description:

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void printUTF8File(string file) {
		setlocale(LC_ALL, "");
		
        locale utf8locale("de_DE.UTF-8");

        wifstream utf_8_text(file.c_str());
        utf_8_text.imbue(utf8locale);

        wstring utf_8_string;
        getline(utf_8_text, utf_8_string);

        wcout << L"Encoding:" << L"UTF-8" << endl;
        wcout << L"Anzahl der Zeichen:" << utf_8_string.size() << endl;
        wcout << utf_8_string << endl;
        for (int i = 0; i < utf_8_string.size() ; i++) {

                wcout << (int) utf_8_string.at(i) << "\t";
                wcout << utf_8_string.at(i) << endl;
        }
}

int main() {
        printUTF8File("data/utf-8_umlaut_short.txt");
}
