// file: Internationalisation/WcharTests.cpp
// description: Test einiger Charactertypen in 
// Abhängigkeit des Locales

#include<iostream>
#include<fstream>
#include<locale>

using namespace std;

int main() {

    wstring str;
    ifstream ifstr;
	
    setlocale(LC_ALL, "");

    while (wcout << ">>" << flush, getline(wcin, str)) {
        // Test for uppercase:
        if (iswupper(str[0])) {
            //oder: if( isupper( str[0], locale() ) )
            wcout << str[0] << " is upper case" << endl;
            wcout << "to lowercase:  ";
            wcout << (wchar_t)towlower(str[0]) << endl;
            //oder: (wchar_t)toupper( str[0], locale() )
        }

        if (iswlower(str[0])) {
            //oder:  if( islower( str[0], locale() ) )
            wcout << str[0] << " is lower case" << endl;
            wcout << "to uppercase :  ";
            wcout << (wchar_t)towupper(str[0]) << endl;
            // oder : (wchar_t)toupper( str[0], locale() )
        }

        if (iswalnum(str[0])) {
            //oder:  if( isalnum( str[0], locale() ) )
            wcout << str[0] << " is alnum Char" << endl;
        }

        if (iswalpha(str[0])) {
            //oder: if( isalpha( str[0], locale() )
            wcout << str[0] << " is alpha Char " << endl;
        }

        if (iswdigit(str[0])) {
            //oder: if( isdigit( str[0], locale() ) )
            wcout << str[0] << " is digit Char " << endl;
        }

        if (iswpunct(str[0])) {
            //oder: if( ispunct( str[0], locale() ) )
            wcout << str[0] << " is punct Char " << endl;
        }

        if (iswspace(str[0])) {
            //oder: if( isspace( str[0], locale() ) )
            wcout << str[0] << " is space Char " << endl;
        }

    }
}
